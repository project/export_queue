<?php

/**
 * @file
 * Administrative pages and forms for the export queues.
 */


// Build the settings form for the export queues on the site.
function export_queue_settings_form() {
  $form = array();

  $form['export_queue_cron_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom cron URL key'),
    '#description' => t('This key must be appended to any export queue custom cron URL.') .'<br />'. t('Changing this value will require any custom cron jobs to be reset.'),
    '#default_value' => export_queue_cron_key(),
    '#required' => TRUE,
    '#size' => 32,
  );

  $form['export_queue_log_exports'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log successful exports to the watchdog.'),
    '#default_value' => variable_get('export_queue_log_exports', FALSE),
  );
  $form['export_queue_log_failures'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log failed exports to the watchdog.'),
    '#default_value' => variable_get('export_queue_log_failures', TRUE),
  );

  $form['queues'] = array(
    '#tree' => TRUE,
  );

  // Add a fieldset for each export queue on the site.
  foreach (export_queue_list() as $queue_id => $queue) {
    $form['queues'][$queue_id] = array(
      '#type' => 'fieldset',
      '#title' => $queue['name'],
      '#description' => $queue['description'],
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['queues'][$queue_id]['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Actively export items in this queue.'),
      '#description' => t('If unchecked, items may still be added to the queue, but they will not be exported.'),
      '#default_value' => $queue['enabled'],
    );

    $form['queues'][$queue_id]['cron'] = array(
      '#type' => 'radios',
      '#title' => t('Cron processing'),
      '#description' => t('Specify whether to process export queues through the normal site cron job or a custom one.') .'<br />'. t("This queue's custom URL is: "). url('export-queue/'. $queue_id .'/'. export_queue_cron_key(), array('absolute' => TRUE)),
      '#options' => array(
        'site' => t('Process this queue through the sitewide cron job.'),
        'custom' => t('Process this queue through a custom cron job targeting the URL below.'),
      ),
      '#default_value' => $queue['cron'],
    );

    // Build the description for this queue's export limit.
    $description = t('Specify how many queued items should be exported at a time with a whole number greater than 0.') .'<br />';

    // Add batch information to the description.
    if ($queue['batch_limit'] > 0) {
      $description .= t('This queue can batch up to @limit items in one request.', array('@limit' => $queue['batch_limit']));
    }
    elseif ($queue['batch_limit'] == -1) {
      $description .= t('This queue can batch an unlimited number of items in one request.');
    }
    else {
      $description .= t('This queue cannot batch items for export.');
    }

    $form['queues'][$queue_id]['cron_limit'] = array(
      '#type' => 'textfield',
      '#title' => t('Cron export limit'),
      '#description' => $description,
      '#default_value' => $queue['cron_limit'],
      '#required' => TRUE,
      '#size' => 16,
    );
  }

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
    '#validate' => array('export_queue_settings_form_save_validate'),
    '#submit' => array('export_queue_settings_form_save_submit'),
  );
  $form['buttons']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset to defaults'),
    '#submit' => array('export_queue_settings_form_reset_submit'),
  );

  return $form;
}

// Validate handler for the export queue settings form's save button.
function export_queue_settings_form_save_validate($form, &$form_state) {
  // Ensure a valid number has been entered for each queue's cron export limit.
  foreach (element_children($form_state['values']['queues']) as $queue_id) {
    if (intval($form_state['values']['queues'][$queue_id]['cron_limit']) == 0) {
      form_set_error('queues]['. $queue_id .'][cron_limit', t('Cron export limits must be whole numbers greater than 0.'));
    }
  }
}

// Submit handler for the export queue settings form's save button.
function export_queue_settings_form_save_submit($form, &$form_state) {
  // Loop through and update all the export queues.
  foreach (element_children($form_state['values']['queues']) as $queue_id) {
    // Load and update the queue with values from the form.
    $queue = $form_state['values']['queues'][$queue_id] + export_queue_load($queue_id);

    export_queue_save($queue);
  }
}

// Submit handler for the export queue settings form's reset button.
function export_queue_settings_form_reset_submit($form, &$form_state) {
  // Delete the variables that hold data that may be reset.
  variable_del('export_queue_log_exports');
  variable_del('export_queue_log_failures');
  variable_del('export_queues');
}

