<?php

/**
 * @file
 * Allows modules to queue objects for export to third party applications.
 */


// Define statuses for items in export queues.
define('EXPORT_QUEUE_PENDING', 0);
define('EXPORT_QUEUE_PROCESSING', 10);
define('EXPORT_QUEUE_COMPLETE', 100);

/**
 * Implementation of hook_menu().
 */
function export_queue_menu() {
  $items = array();

  $items['admin/settings/export-queues'] = array(
    'title' => 'Export queues',
    'description' => "Configure your site's export queues.",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('export_queue_settings_form'),
    'access arguments' => array('administer queues'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'export_queue.admin.inc',
  );

  $items['export-queue/%export_queue/%'] = array(
    'title' => 'Process queue',
    'description' => 'Process the specified export queue.',
    'page callback' => 'export_queue_process',
    'page arguments' => array(1),
    'access callback' => 'export_queue_cron_access',
    'access arguments' => array(1, 2),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

// Determine access to an export queue's custom cron URL.
function export_queue_cron_access($queue, $key) {
  // Allow access if the queue is enabled, the queue is set to use the custom
  // cron URL, and the cron key specified in the URL is correct.
  return $queue['enabled'] && $queue['cron'] == 'custom' && !strcmp($key, export_queue_cron_key());
}

/**
 * Implementation of hook_perm().
 */
function export_queue_perm() {
  return array('administer queues');
}

/**
 * Implementation of hook_enable().
 */
function export_queue_enable() {
  // If a custom cron key has not been set or is missing, set one now.
  if (export_queue_cron_key() == '') {
    variable_set('export_queue_cron_key', substr(uniqid(), 0, 8));
  }
}

/**
 * Implementation of hook_cron().
 */
function export_queue_cron() {
  // Process any active queues working on the site-wide cron.
  foreach (export_queue_list(array('enabled' => TRUE, 'cron' => 'site')) as $queue_id => $queue) {
    export_queue_process($queue);
  }
}

// Return the cron key used in custom export queue cron job URLs.
function export_queue_cron_key() {
  return variable_get('export_queue_cron_key', '');
}

/**
 * Return a specific export queue or a sorted array of all export queues.
 *
 * @param $filter
 *   An array of attributes to filter the returned list by; for example, if you
 *     specify 'id' => 'my_queue' in the array, then only the export queue with
 *     an id equal to 'my_queue' would be included.
 * @return
 *   The array of export queues sorted alphabetically by name and keyed by ID.
 */
function export_queue_list($filter = array()) {
  static $queues = array();

  // Generate a unique ID for the filter.
  $filter_id = md5(serialize($filter));

  // If the queues haven't been defined for this filter yet...
  if (!isset($queues[$filter_id])) {
    $saved_queues = variable_get('export_queues', array());
    $temp = array();

    // Update the array with the values from the settings form and filter out
    // any that don't match the filter criteria.
    foreach (module_invoke_all('export_queue') as $queue_id => $queue) {
      $queue['queue_id'] = $queue_id;

      // Default every queue to be enabled.
      if (empty($queue['enabled'])) {
        $queue['enabled'] = TRUE;
      }

      // Default every queue to be processed on the site cron run.
      if (empty($queue['cron'])) {
        $queue['cron'] = 'site';
      }

      // If no batch limit is specified, set it to 0.
      if (empty($queue['batch_limit'])) {
        $queue['batch_limit'] = 0;
      }

      // Update the queue with saved data.
      if (!empty($saved_queues[$queue_id])) {
        $queue = $saved_queues[$queue_id] + $queue;
      }

      // Check the queue against the filter array to determine whether to add it
      // to the queue array or not.
      $valid = TRUE;

      foreach ($filter as $attribute => $value) {
        // If the current value for the specified attribute on the queue does
        // not match the filter value...
        if ($queue[$attribute] != $value) {
          // Do not add it to the temporary array.
          $valid = FALSE;
        }
      }

      if ($valid) {
        $temp[$queue['name']] = $queue;
      }
    }

    // Sort the queues by their human readable name.
    ksort($temp);

    // Build the cached queues array using the sorted array of export queues.
    $queues[$filter_id] = array();

    foreach ($temp as $queue) {
      $queues[$filter_id][$queue['queue_id']] = $queue;
    }
  }

  return $queues[$filter_id];
}

/**
 * Load the data for a specific export queue.
 *
 * @param $queue_id
 *   The machine readable ID of the export queue.
 * @return
 *   The requested export queue array or FALSE if not found.
 */
function export_queue_load($queue_id) {
  // Load a list of export queues filtered by the ID.
  $queues = export_queue_list(array('queue_id' => $queue_id));

  // If the queue actually existed...
  if (!empty($queues)) {
    // Return the queue array.
    return array_shift($queues);
  }

  // Otherwise return FALSE.
  return FALSE;
}

// Save an export queue array to the export queues variable.
function export_queue_save($queue) {
  $queue_id = $queue['queue_id'];

  // Filter out data that will always come from hook_export_queue().
  $queue = array_diff_key($queue, drupal_map_assoc(array('queue_id', 'name', 'description', 'callback', 'batch_limit')));

  // Load and update the export queues array.
  $queues = variable_get('export_queues', array());
  $queues[$queue_id] = $queue;

  variable_set('export_queues', $queues);
}

/**
 * Look for any items awaiting export in the queue and send them on to the
 *   queue's callback for attempted export.
 *
 * @param $queue
 *   A fully loaded export queue array.
 * @return
 *   No return value.
 */
function export_queue_process($queue) {
  // If there are pending items in the queue...
  if (TRUE || db_result(db_query("SELECT COUNT(qid) FROM {export_queue} WHERE queue = '%s' AND status < %d", $queue['queue_id'], EXPORT_QUEUE_PROCESSING))) {
    $items = array();

    // Load up the appropriate number of items based on the queue's settings.
    $result = db_query_range("SELECT * FROM {export_queue} WHERE queue = '%s' AND status < %d ORDER BY priority DESC", $queue['queue_id'], EXPORT_QUEUE_PROCESSING, 0, $queue['cron_limit']);

    // Build an array of items for export and send it to the export callback.
    while ($row = db_fetch_array($result)) {
      $items[$row['qid']] = $row;
    }

    $queue['callback']($items);
  }
}

